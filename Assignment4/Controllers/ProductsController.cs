using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Collections;
using Assignment4.Models;
using PagedList;
using System.Data.SqlClient;
namespace Assignment4.Controllers
{
    public class ProductsController : Controller
    {
        private IPTAssignment4Entities1 db = new IPTAssignment4Entities1();
        public static List<Product> prod = new List<Product>();
        // GET: Products
        public ActionResult Index(string searchOption, string searchQuery, int? page, string sortby)
        {
            //Sorting parameters
            ViewBag.SortByNameParameter = string.IsNullOrEmpty(sortby) ? "ProductName Sorting" : "";

            ViewBag.SortByPriceParameter = sortby == "ProductPrice" ? "ProductPrice Sorting" : "ProductPrice";

            //Product model.
            var product = db.Products.AsQueryable();
            
            //Searching.
            if (searchOption == "Category")
            {
                var catagoryId = db.Categories.Where(c => c.CategoryName.Equals(searchQuery));
                foreach (var cat in catagoryId)
                {
                    product = product.Where(pro => pro.PrimaryCategoryID.Equals(cat.CategoryId) || searchQuery == null).Include(pro => pro.Category);
                }
                prod = product.ToList();
            }
            else
            {
                if (searchOption == "Name")
                {
                    product= product.Where(p => p.ProductName.Contains(searchQuery) || searchQuery == null).Include(p => p.Category);
                    prod = product.ToList();
                }
                else
                {
                    if (searchOption == "Price")
                    {
                        int price = Convert.ToInt32(searchQuery);
                        product = product.Where(p => p.Price <= price || searchQuery == null);
                        prod = product.ToList();
                    }
                    else
                    {
                        if (searchOption == "Color")
                        {
                            
                            var pp = from p in db.Products.SqlQuery("select * from Product where OtherAttributes.exist('Product/OtherAttributes[@Color="+searchQuery+"]') = 1")
                                     select p;
                            product = pp.AsQueryable();
                            prod = product.ToList();

                        }
                        else
                        {
                            product = product.Where(p => p.ProductDescription.Contains(searchQuery) || searchQuery == null).Include(p => p.Category);
                            prod = product.ToList();
                        }
                    }
                    
                }

            }
            // Sorting 
            switch (sortby)
            {
                case "ProductName Sorting":
                    product = product.OrderByDescending(p => p.ProductName);
                    break;

                case "ProductPrice Sorting":
                    product = product.OrderByDescending(p => p.Price);
                    break;

                case "ProductPrice":
                    product = product.OrderBy(p => p.Price);
                    break;
                default:
                    product = product.OrderBy(p => p.ProductName);
                    break;
            }

            return View(product.ToPagedList(page ?? 1, 100));
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.PrimaryCategoryID = new SelectList(db.Categories, "CategoryId", "CategoryName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,ProductName,ProductDescription,PrimaryCategoryID,Price,Active,OtherAttributes")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PrimaryCategoryID = new SelectList(db.Categories, "CategoryId", "CategoryName", product.PrimaryCategoryID);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.PrimaryCategoryID = new SelectList(db.Categories, "CategoryId", "CategoryName", product.PrimaryCategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,ProductDescription,PrimaryCategoryID,Price,Active,OtherAttributes")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PrimaryCategoryID = new SelectList(db.Categories, "CategoryId", "CategoryName", product.PrimaryCategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //Export Summary Action Function.
        public ActionResult ExportSummary(int? page)
        {
            Dictionary<string, string> mylist = new Dictionary<string, string>();

            ViewBag.Total = db.Products.Count();
            foreach(var iterator in prod)
            {
                var categoryResult = db.Products.Count(m => m.Category.CategoryName == iterator.Category.CategoryName);
                var categorySearch = prod.Count(m => m.Category.CategoryName == iterator.Category.CategoryName);
                if(!mylist.ContainsKey(iterator.Category.CategoryName))
                mylist.Add(iterator.Category.CategoryName, categoryResult + "+" + categorySearch);
                ViewBag.List = mylist;      
            }
            return View("ExportSummary");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
